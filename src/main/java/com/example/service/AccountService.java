package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.repository.AccountRepository;

@Service
public class AccountService {

	@Autowired
	AccountRepository accRepository;

	public String findUsername(String userName) {
		return accRepository.findUsername(userName);
	}
	
	public String findGakka(String userName) {
		return accRepository.findGakka(userName);
	}
	
	public String findKurasu(String userName) {
		return accRepository.findKurasu(userName);
	}
	
	public String findGakunen(String userName) {
		return accRepository.findGakunen(userName);
	}
	
	public String findBangou(String userName) {
		return accRepository.findBangou(userName);
	}

	public String findMailaddress(String userName) {
		return accRepository.findMailaddress(userName);
	}
	
}
