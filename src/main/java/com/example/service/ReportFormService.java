package com.example.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dto.UserRequestReportForm;
import com.example.entity.Account;
import com.example.entity.ReportForm;
import com.example.repository.ReportFormRepository;

@Service
public class ReportFormService {
	
	@Autowired
	ReportFormRepository reportFormRepository;
	
	public void create(UserRequestReportForm userRequestRepForm,Account account) {
		reportFormRepository.save(CreateReportForm(userRequestRepForm,account));
	}
	
	private ReportForm CreateReportForm(UserRequestReportForm userRequestRepForm,Account account) {

		ReportForm repForm = new ReportForm();
		
		LocalDateTime d = LocalDateTime.now();
		DateTimeFormatter df1 = 
				DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
		String s = df1.format(d);
		repForm.setCurrent(s);
		
		repForm.setChecks(userRequestRepForm.getChecks());
		repForm.setKigyoumei(userRequestRepForm.getKigyoumei());
		repForm.setSanmoji(userRequestRepForm.getSanmoji());
		
		if (userRequestRepForm.getHiniti().equals("")) {
			repForm.setHiniti("9999-12-31");
		} else {
			repForm.setHiniti(userRequestRepForm.getHiniti());
		}
		
		repForm.setStarttime(userRequestRepForm.getStarttime());
		repForm.setEndtime(userRequestRepForm.getEndtime());
		repForm.setJyukenbasyo(userRequestRepForm.getJyukenbasyo());
		repForm.setNaiyou1(userRequestRepForm.getNaiyou1());
		repForm.setNaiyou2(userRequestRepForm.getNaiyou2());
		repForm.setNaiyou3(userRequestRepForm.getNaiyou3());
		repForm.setKekka(userRequestRepForm.getKekka());
		repForm.setNinzu(userRequestRepForm.getNinzu());
		repForm.setJikan(userRequestRepForm.getJikan());
		repForm.setSyutudainaiyou(userRequestRepForm.getSyutudainaiyou());
		repForm.setKomento(userRequestRepForm.getKomento());
		repForm.setAccount(account);
		repForm.setRepstatus("0");
		repForm.setPermissionrefusal("申請中");
		return repForm;
	}
	
	public List<ReportForm> findAlluserid(String userName) {
		return reportFormRepository.findAlluserid(userName);
	}

	public ReportForm getOne(String id) {
		return reportFormRepository.getOne(id);
	}

	public void delete(String id) {
		reportFormRepository.delete(getOne(id));
	}

	public ReportForm save(ReportForm repForm, Account account) {
		repForm.setAccount(account);
		return reportFormRepository.save(repForm);	
	}
	
	public ReportForm update(ReportForm repForm, Account account) {
		repForm.setAccount(account);
		return reportFormRepository.save(repForm);	
	}

	public List<ReportForm> findAllsanmoji(String sanmoji) {
		return reportFormRepository.findAllsanmoji(sanmoji);
	}

	public List<ReportForm> findAllstatus() {
		
		return reportFormRepository.findAllstatus();
	}

	public Integer saveKomento(String komento, String id) {
		return reportFormRepository.saveKomento(komento,id);
	}
	
	public Integer saveUpdateStatus(String repstatus, String permissionrefusal, String id) {
		return reportFormRepository.saveUpdateStatus(repstatus,permissionrefusal,id);	
	}

	public String getUserid(String id) {
		return reportFormRepository.getUserid(id);
	}

}
