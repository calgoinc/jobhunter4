package com.example.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.entity.ReportForm;

@Transactional
@Repository
public interface ReportFormRepository extends JpaRepository<ReportForm,String>{

	@Query(value = "SELECT * FROM REPORTFORM WHERE USERID = ? AND REPSTATUS = 0", nativeQuery = true)
	List<ReportForm> findAlluserid(String userName);
	
	@Query(value = "SELECT * FROM REPORTFORM WHERE SANMOJI = ? AND REPSTATUS = 1", nativeQuery = true)
	List<ReportForm> findAllsanmoji(String sanmoji);

	@Query(value = "SELECT * FROM REPORTFORM ", nativeQuery = true)
	List<ReportForm> findAllstatus();

	@Modifying //update applicationform set komento='申請許可 ' where id='1';
	@Query(value = "UPDATE REPORTFORM SET KOMENTO = ? WHERE ID = ?", nativeQuery = true)
	Integer saveKomento(String komento, String id);
	
	@Modifying
	@Query(value = "UPDATE REPORTFORM SET REPSTATUS = ?,PERMISSIONREFUSAL = ? WHERE ID = ?", nativeQuery = true)
	Integer saveUpdateStatus(String repstatus, String permissionrefusal, String id);
	
	@Query(value = "SELECT USERID FROM REPORTFORM WHERE ID = ?", nativeQuery = true)
	String getUserid(String id);
}
	

