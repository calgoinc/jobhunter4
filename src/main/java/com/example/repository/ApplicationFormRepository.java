package com.example.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.entity.ApplicationForm;

@Transactional
@Repository
public interface ApplicationFormRepository extends JpaRepository<ApplicationForm,String>{
	
	@Query(value = "SELECT * FROM APPLICATIONFORM WHERE USERID = ? AND APPSTATUS = 0", nativeQuery = true)
	List<ApplicationForm> findAlluserid(String userName);
	
	@Query(value = "SELECT * FROM APPLICATIONFORM ", nativeQuery = true)
	List<ApplicationForm> findAllstatus();

	@Modifying //update applicationform set komento='申請許可 ' where id='1';
	@Query(value = "UPDATE APPLICATIONFORM SET KOMENTO = ? WHERE ID = ?", nativeQuery = true)
	Integer saveKomento(String komento,String id);

	@Modifying
	@Query(value = "UPDATE APPLICATIONFORM SET APPSTATUS = ?,PERMISSIONREFUSAL = ? WHERE ID = ?", nativeQuery = true)
	Integer saveUpdateStatus(String appstatus, String permissionrefusal, String id);

	@Query(value = "SELECT * FROM APPLICATIONFORM WHERE USERID = ? AND APPSTATUS = 1", nativeQuery = true)
	List<ApplicationForm> findAlluseridcomp(String userName);

	@Query(value = "SELECT USERID FROM APPLICATIONFORM WHERE ID = ?", nativeQuery = true)
	String getUserid(String id);

}
