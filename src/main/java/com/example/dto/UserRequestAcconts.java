package com.example.dto;

import java.io.Serializable;

public class UserRequestAcconts implements Serializable {

	private static final long serialVersionUID = 1L;

	private String userid;
	private String password;
	private String email;
	private String fullname;
	private String subject;
	private String kurasu;
	private String number;


	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getKurasu() {
		return kurasu;
	}

	public void setKurasu(String kurasu) {
		this.kurasu = kurasu;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

}
