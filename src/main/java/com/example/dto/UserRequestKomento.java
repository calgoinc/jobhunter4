package com.example.dto;

import java.io.Serializable;

public class UserRequestKomento implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String komento;

	public String getKomento() {
		return komento;
	}

	public void setKomento(String komento) {
		this.komento = komento;
	}
	
	

}
