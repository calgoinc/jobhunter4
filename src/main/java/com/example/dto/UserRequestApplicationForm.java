package com.example.dto;

public class UserRequestApplicationForm {
	
	private String hiniti;
	private String current;
	private String starttime;
	private String endtime;
	private String basyo;
	private String naiyou;
	private String kigyoumei1;
	private String kigyoumei2;
	private String kigyoumei3;
	private String kessekibi;
	private String soutaibi;
	private String soutaijikan;
	private String tikokubi;
	private String tikokujikan;
	private String memo;
	private String komento;
	private String appstatus;
	
	public String getHiniti() {
		return hiniti;
	}
	public void setHiniti(String hiniti) {
		this.hiniti = hiniti;
	}
	public String getCurrent() {
		return current;
	}
	public void setCurrent(String current) {
		this.current = current;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	public String getBasyo() {
		return basyo;
	}
	public void setBasyo(String basyo) {
		this.basyo = basyo;
	}
	public String getNaiyou() {
		return naiyou;
	}
	public void setNaiyou(String naiyou) {
		this.naiyou = naiyou;
	}
	public String getKigyoumei1() {
		return kigyoumei1;
	}
	public void setKigyoumei1(String kigyoumei1) {
		this.kigyoumei1 = kigyoumei1;
	}
	public String getKigyoumei2() {
		return kigyoumei2;
	}
	public void setKigyoumei2(String kigyoumei2) {
		this.kigyoumei2 = kigyoumei2;
	}
	public String getKigyoumei3() {
		return kigyoumei3;
	}
	public void setKigyoumei3(String kigyoumei3) {
		this.kigyoumei3 = kigyoumei3;
	}
	public String getKessekibi() {
		return kessekibi;
	}
	public void setKessekibi(String kessekibi) {
		this.kessekibi = kessekibi;
	}
	public String getSoutaibi() {
		return soutaibi;
	}
	public void setSoutaibi(String soutaibi) {
		this.soutaibi = soutaibi;
	}
	public String getSoutaijikan() {
		return soutaijikan;
	}
	public void setSoutaijikan(String soutaijikan) {
		this.soutaijikan = soutaijikan;
	}
	public String getTikokubi() {
		return tikokubi;
	}
	public void setTikokubi(String tikokubi) {
		this.tikokubi = tikokubi;
	}
	public String getTikokujikan() {
		return tikokujikan;
	}
	public void setTikokujikan(String tikokujikan) {
		this.tikokujikan = tikokujikan;
	}
	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}
	public String getKomento() {
		return komento;
	}
	public void setKomento(String komento) {
		this.komento = komento;
	}
	public String getAppstatus() {
		return appstatus;
	}
	public void setAppstatus(String appstatus) {
		this.appstatus = appstatus;
	}
	

}
