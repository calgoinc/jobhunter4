package com.example.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.domain.DbAccountDetails;
import com.example.entity.ApplicationForm;
import com.example.repository.AccountRepository;
import com.example.service.AccountService;
import com.example.service.ApplicationFormService;

@Controller
public class P4Controller {
	
	private final JavaMailSender javaMailSender;
    @Autowired
    P4Controller(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }
	
	@Autowired
	ApplicationFormService applicationFormService;
	
	@Autowired
	AccountService accService;
	
	@Autowired
	AccountRepository accountRepository;

	@GetMapping("/p4fail")
	String p4fail() {
		return "p4/p4fail";
	}

	@RequestMapping(value = "/p4main", method = RequestMethod.GET)
	public String p4main(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		String userName = auth.getName();
		String judgmenttea = accountRepository.judgmentteacher(userName);
		if (judgmenttea.equals("0")) {
			String Name =accountRepository.findName(userName);		
			model.addAttribute("Name", Name);
			model.addAttribute("error","その機能は使えません！");
			return "menu/menu1";
		}else {
		List<ApplicationForm> Applist = applicationFormService.findAllstatus();
		model.addAttribute("Applist", Applist);
		return "p4/p4main";
		}
	}
	
	@GetMapping("/p4/{id}/p4success")//内容確認
	  public String edit(@PathVariable String id,Model model) {
		  String userName = applicationFormService.getUserid(id);
		  ApplicationForm Appform = applicationFormService.getOne(id); 
		  model.addAttribute("account",accService.findGakka(userName)+
				  accService.findGakunen(userName)+
				  accService.findKurasu(userName)+
				  accService.findBangou(userName)+" "+
				  accService.findUsername(userName));
		  model.addAttribute("Appform",Appform);
	  return "p4/p4success"; 
	  }

	@RequestMapping(value = "/p4/{id}", params = "status=permission" ,method = RequestMethod.POST)//更新
	public String updatepermission(@PathVariable String id,@ModelAttribute("komento") String komento
			,ApplicationForm appForm,@AuthenticationPrincipal DbAccountDetails dbAccountDetails) {
		String Appstatus = "1";
		String Permissionrefusal= "承認済み";
		applicationFormService.saveUpdateStatus(Appstatus,Permissionrefusal,id);
		applicationFormService.saveKomento(komento,id);
		String userName = applicationFormService.getUserid(id);
		String mail = accService.findMailaddress(userName);
		SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(mail);
        mailMessage.setFrom("calgo.jp@gmail.com");
        mailMessage.setSubject("教員がコメントを追加しました");
        mailMessage.setText("申請が承認されました。コメントを確認してください。");
        javaMailSender.send(mailMessage);
		return "redirect:/p4main";
	}
	
	@RequestMapping(value = "/p4/{id}", params = "status=refusal" ,method = RequestMethod.POST)//更新
	public String updaterefusal(@PathVariable String id,@ModelAttribute("komento") String komento) {
		String Appstatus = "0";
		String Permissionrefusal= "承認拒否";
		applicationFormService.saveUpdateStatus(Appstatus,Permissionrefusal,id);
		applicationFormService.saveKomento(komento,id);
		String userName = applicationFormService.getUserid(id);
		String mail = accService.findMailaddress(userName);
		SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(mail);
        mailMessage.setFrom("calgo.jp@gmail.com");
        mailMessage.setSubject("教員がコメントを追加しました");
        mailMessage.setText("申請が拒否されたため、コメントを確認し再度申請してください。");
        javaMailSender.send(mailMessage);
		return "redirect:/p4main";
	}

}
