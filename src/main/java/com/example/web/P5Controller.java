package com.example.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.entity.ReportForm;
import com.example.repository.AccountRepository;
import com.example.service.AccountService;
import com.example.service.ReportFormService;

@Controller
public class P5Controller {
	
	private final JavaMailSender javaMailSender;
    @Autowired
    P5Controller(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }
	
	@Autowired
	ReportFormService reportFormService;
	
	@Autowired
	AccountService accService;
	
	@Autowired
	AccountRepository accountRepository;

	@GetMapping("/p5fail")
	String p5fail() {
		return "p5/p5fail";
	}
	
	@RequestMapping(value = "/p5main", method = RequestMethod.GET)
	public String p5main(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		String userName = auth.getName();
		String judgmenttea = accountRepository.judgmentteacher(userName);
		if (judgmenttea.equals("0")) {
			String Name =accountRepository.findName(userName);		
			model.addAttribute("Name", Name);
			model.addAttribute("error","その機能は使えません！");
			return "menu/menu1";
		}else {
		List<ReportForm> Replist = reportFormService.findAllstatus();
		model.addAttribute("Replist", Replist);
		return "p5/p5main";
		}//
	}
	
	@GetMapping("/p5/{id}/p5success")//内容確認
	  public String edit(@PathVariable String id,Model model) {
		  String userName = reportFormService.getUserid(id);
		  ReportForm Repform = reportFormService.getOne(id);
		  model.addAttribute("account",accService.findGakka(userName)+
				  accService.findGakunen(userName)+
				  accService.findKurasu(userName)+
				  accService.findBangou(userName)+" "+
				  accService.findUsername(userName));
		  model.addAttribute("Repform",Repform);
	  return "p5/p5success"; 
	  }

	@RequestMapping(value = "/p5/{id}", params = "status=permission" ,method = RequestMethod.POST)//更新
	public String updatepermission(@PathVariable String id,@ModelAttribute("komento") String komento) {
		String Repstatus = "1";
		String Permissionrefusal= "承認済み";
		reportFormService.saveUpdateStatus(Repstatus,Permissionrefusal,id);
		reportFormService.saveKomento(komento,id);
		String userName = reportFormService.getUserid(id);
		String mail = accService.findMailaddress(userName);
		SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(mail);
        mailMessage.setFrom("calgo.jp@gmail.com");
        mailMessage.setSubject("教員がコメントを追加しました");
        mailMessage.setText("申請が承認されました。コメントを確認してください。");
        javaMailSender.send(mailMessage);
		return "redirect:/p5main";
	}
	
	@RequestMapping(value = "/p5/{id}", params = "status=refusal" ,method = RequestMethod.POST)//更新
	public String updaterefusal(@PathVariable String id,@ModelAttribute("komento") String komento) {
		String Repstatus = "0";
		String Permissionrefusal= "承認拒否";
		reportFormService.saveUpdateStatus(Repstatus,Permissionrefusal,id);
		reportFormService.saveKomento(komento,id);
		String userName = reportFormService.getUserid(id);
		String mail = accService.findMailaddress(userName);
		SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(mail);
        mailMessage.setFrom("calgo.jp@gmail.com");
        mailMessage.setSubject("教員がコメントを追加しました");
        mailMessage.setText("申請が拒否されたため、コメントを確認し再度申請してください。");
        javaMailSender.send(mailMessage);
		return "redirect:/p5main";
	}

}
