package com.example.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.example.repository.AccountRepository;

@Controller
public class MenuController {
	@Autowired
	AccountRepository accountRepository;

	
	@RequestMapping(value = "/menu1", method = RequestMethod.GET)
	public String p1main(Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		String userName = auth.getName();
		String Name =accountRepository.findName(userName);
		
		model.addAttribute("Name", Name);

		return "menu/menu1";
		}
}
