package com.example.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.entity.ReportForm;
import com.example.service.ReportFormService;

@Controller
public class P3Controller {
	@Autowired
	ReportFormService reportFormService;

	@RequestMapping(value = "/p3/{id}/p3result", method = RequestMethod.GET)
	public String p3result(@PathVariable String id,Model model) {
		
		ReportForm Repform = reportFormService.getOne(id); 
		  model.addAttribute("Repform",Repform);
		
		return "p3/p3result";
	}
	
	@RequestMapping(value = "/p3main", method = RequestMethod.GET)
	public String p1main(Model model,@ModelAttribute("sanmoji") String sanmoji) {
		
		List<ReportForm> ReportFormList = reportFormService.findAllsanmoji(sanmoji);
		model.addAttribute("ReportFormList", ReportFormList);
		return "p3/p3main";
	}

}
